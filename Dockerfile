# Use a Python base image
FROM python:3.11-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the contents of the local directory to the container
COPY . .

# Install Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Expose port 3000 for Flask
EXPOSE 3000

# Start Flask server
CMD ["python", "main.py"]