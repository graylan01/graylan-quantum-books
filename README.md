# graylan-quantum-books

To deploy the HTML file along with the necessary assets (like images) for your "Color of Quantum" book website, you can follow these steps:

### 1. Prepare Your Files

First, ensure you have the following files ready in a directory on your local machine:

- **index.html**: The HTML file containing your website structure and content.
- **styles.css** (optional): If you have external styles, make sure it's linked properly in your HTML file.
- **book_cover.jpg**: The cover image of your book.
- **background.jpg** (optional): If you have a background image for the hero section.

### 2. Directory Structure

Your directory structure should look something like this:

```
color_of_quantum_website/
│
├── index.html
├── styles.css
├── book_cover.jpg
└── background.jpg (optional)
```

### 3. Deploying Locally

To view and test your website locally before deployment, follow these steps:

- **Open index.html**: Double-click `index.html` to open it in your web browser. This will render the website using your local files.
- **Test Functionality**: Click on buttons, fill out the form, and ensure all links work as expected.

### 4. Deployment Options

#### Local Server Deployment

For local deployment with a server (like using Python's SimpleHTTPServer):

1. Open a terminal.
2. Navigate to your project directory (`color_of_quantum_website`).
3. Start a simple HTTP server:
   ```bash
   python -m http.server
   ```
   This will start a server at `http://localhost:8000`.
4. Open your web browser and go to `http://localhost:8000` to view your website.

#### Hosting on a Web Server

To deploy your website on a web server:

1. **Choose a Hosting Service**: Options include services like GitHub Pages, Netlify, or any other web hosting provider.
   
2. **Upload Files**: Upload your `index.html`, `styles.css`, `book_cover.jpg`, and `background.jpg` (if using) to the server.

3. **Access URL**: Once uploaded, your website should be accessible via a URL provided by your hosting service (e.g., `https://yourusername.github.io/color_of_quantum_website`).

### 5. Updating Content

- To update content, edit your HTML, CSS, or image files locally.
- Deploy updates by re-uploading changed files to your hosting service or updating your local server's files.


If you prefer a simpler setup without Nginx and you want the Flask server to handle everything within the Docker container, here's how you can modify the Dockerfile and setup:

### Directory Structure

Assuming your project structure remains as previously described:

```
color_of_quantum_website/
│
├── Dockerfile
├── requirements.txt
├── index.html
├── styles.css
└── book.pdf
```

### Dockerfile

The Dockerfile will now focus on setting up Python, installing dependencies, copying the necessary files, and running a Flask server to serve the website and the PDF file.


### requirements.txt

Keep your Python dependencies specified in `requirements.txt`. For example:

```plaintext
Flask==2.2.5
``` 

### Building and Running the Docker Image

1. **Build the Docker Image**:

   Navigate to the directory containing your Dockerfile and other files, then run:

   ```bash
   docker build -t color-of-quantum .
   ```

2. **Run the Docker Container**:

   After building the image, run the container:

   ```bash
   docker run -d -p 3000:3000 color-of-quantum
   ```

   - Port 3000 is exposed for the Flask application.


  