from flask import Flask, send_file, jsonify
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import os
import hashlib

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0  # Disable caching

limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["10 per minute"]
)

# Function to calculate checksum of a file
def calculate_checksum(filepath):
    hasher = hashlib.md5()
    with open(filepath, 'rb') as f:
        buf = f.read()
        hasher.update(buf)
    return hasher.hexdigest()

# Endpoint to serve the PDF file
@app.route('/get-pdf', methods=['GET'])
@limiter.limit("1 per minute")  # Rate limit: 1 request per minute
def get_pdf():
    try:
        pdf_file = '/static/book.pdf'  # Replace with the path to your PDF file
        if not os.path.exists(pdf_file):
            return jsonify(error='PDF file not found'), 404
        
        # Verify checksum for security
        expected_checksum = '04f4239966dcbfb4aa29a5003c0f2b82'  # Replace with your pre-calculated checksum
        file_checksum = calculate_checksum(pdf_file)
        if file_checksum != expected_checksum:
            return jsonify(error='Checksum verification failed'), 500
        
        # Serve the PDF file
        return send_file(pdf_file, attachment_filename='color_of_quantum.pdf')
    
    except Exception as e:
        return jsonify(error=str(e)), 500

# Endpoint to serve the book content (sample implementation)
@app.route('/get-book', methods=['GET'])
@limiter.limit("5 per minute")  # Rate limit: 5 requests per minute
def get_book():
    try:
        book_file = 'path/to/your/book.pdf'  # Replace with the path to your book PDF file
        if not os.path.exists(book_file):
            return jsonify(error='Book file not found'), 404
        
        # Serve the book file
        return send_file(book_file)
    
    except Exception as e:
        return jsonify(error=str(e)), 500

# Error handler for 404 Not Found errors
@app.errorhandler(404)
def not_found_error(e):
    return jsonify(error='Resource not found'), 404

# Error handler for 429 Too Many Requests (rate limiting)
@app.errorhandler(429)
def rate_limit_exceeded(e):
    return jsonify(error='Rate limit exceeded. Please try again later.'), 429

if __name__ == '__main__':
    app.run(debug=True)